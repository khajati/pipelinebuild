import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

final class Checker {
    @Test
    void sum() {
        AppMath math = new AppMath();
        int value = math.finalValue();
        assertEquals (51, value);
    }
}
