import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class AppMath {
    int finalValue() {
        // Instantiate ArrayList
        List<Integer> numList = new ArrayList<Integer>();

        int value = 0;
        int lastValue;
        //Add to numList
        numList.add(4);
        numList.add(51);
        numList.add(23);
        numList.add(11);
        numList.add(22);
        numList.add(33);

        // Iterator - traversing list and set
        Iterator<Integer> itr = numList.iterator();

        // Prints out the entire list
        while (itr.hasNext()) {
            value = itr.next();
            System.out.println("Value: " + value);
            if (value >= 4 && value <= 49) {
                itr.remove();
            }
        }
        lastValue = numList.get(0);
        return lastValue;
    }
}

